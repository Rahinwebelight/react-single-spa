const path = require("path");

module.exports = () => {
  const defaultConfig = {
    entry:  "./src/Test.tsx",
    mode: 'development',
    output: {
      path: path.resolve(__dirname, 'dist'),
      publicPath: '/dist/',
      filename: 'bundle.js'
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx|ts|tsx)?$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        },
        {
          test: /\.(jpe?g|gif|png|ico)$/,
          use: ['file-loader?name=[name].[ext]']
        },
      ],
    },
    resolve: {
      extensions: ['.js', '.jsx', '.ts', '.tsx'],
    },
    // devServer: {
    //   publicPath: "/",
    //   contentBase: "./src",  
    //   hot: true
    // },
    externals: [
      "react",
      "react-dom",
      //   "single-spa",
    ],
  };
  return defaultConfig;
};
